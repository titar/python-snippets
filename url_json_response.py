# -*- coding: utf-8 -*-

import sys
import urllib2
import json


def main():
        countries = {}

        # task 1
        try:
                f = open('ips.txt')
        except:
                print('File not exist.')
                sys.exit(0)

        for line in f:

                ip_str = line.strip()
                resp = process_ip(ip_str)

                if not 'country' in resp:
                        print('WARNING! JSON response has no key "country"')
                        continue

                # task 2
                if resp['country'] in countries:
                        countries[resp['country']].append(ip_str)
                else:
                        countries[resp['country']] = [ip_str]
        f.close()

        print('RESULTS:')
        for country in countries:
                print("%s: %s" % (country, ','.join(countries[country])))


def process_ip(ip):
        try:
                print('Proccessing an ip : ', ip)
                url = 'http://ip-api.com/json/%s' % (ip,)
                print(url)
                response = urllib2.urlopen('http://ip-api.com/json/%s' % (ip,))
                content = response.read()
                return json.loads(content)
        except urllib2.HTTPError, e:
                print('Failed response:')
                print(e)
                return []
        except:
                print('Smthng wrong with ip: ', ip)
                return []


if __name__ == '__main__':
        main()